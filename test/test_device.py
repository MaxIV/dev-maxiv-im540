# Imports
from time import sleep
from mock import MagicMock, patch
from im540dev import device
from PyTango import DevFailed, DevState, AttrQuality
from devicetest import DeviceTestCase
from random import randrange

READ_PERIOD = 0.1

# Device test case
class Im540DeviceTestCase(DeviceTestCase):
    """Test case for packet generation."""

    device = device.Im540Device
    properties = {
        'Host': "192.168.127.254",
        'Port': 4001,
        'ReadPeriod': READ_PERIOD
    }

    @classmethod
    def mocking(cls):
        """Mock the im540 controller access."""
        device.Im540Controller = cls.Im540Controller = MagicMock()
        device.Im540Exception = cls.Im540Exception = MagicMock()

        cls.controller = cls.Im540Controller.return_value
        cls.controller.connect.return_value = None
        cls.controller.disconnect.return_value = None
        cls.controller.read_pressure.return_value = (1.65e-10, 0, "", 0, "")
        cls.controller.is_on.return_value = False
        cls.controller.write_emission_current.return_value = (0, "0- AUTO")
        cls.controller.read_emission_current.return_value = (0, "0- AUTO")
        cls.controller.reset_errors.return_value = None
        cls.controller.turn_on.return_value = (1, 1)
        cls.controller.turn_off.return_value = (1, 0)
        cls.controller.global_device_errors.return_value = 0, ""
        cls.controller.ioni_supply_errors.return_value = 0, ""
        cls.controller.ioni_supply_warnings.return_value = 0, ""
        cls.controller.voltage_supply_errors.return_value = 0, ""
        cls.controller.voltage_supply_warnings.return_value = 0, ""

    def assertState(self, expected):
        """ Check device state """
        present = self.device.state()
        self.assertEqual(present, expected,
                         "present state: %s, expected state: %s" % (
                         present, expected))

    def assertStatus(self, expected):
        """ Check device status """
        present = self.device.status()
        self.assertEqual(present, expected,
                         "present status: %s, expected status: %s" % (
                         present, expected))

    def test_properties(self):
        """ Test init device and check properties usage """
        self.assertState(DevState.OFF)
        self.Im540Controller.assert_called_with("192.168.127.254", 4001)

    def test_turn_on_and_off(self):
        """ test commands turnOn and turnOff """
        self.assertState(DevState.OFF)
        self.device.TurnOn()
        self.assertState(DevState.ON)
        self.assertStatus("Device is in ON state")
        self.device.TurnOff()
        self.assertState(DevState.OFF)
        self.assertStatus("Device is in OFF state")

    def test_reset_errors(self):
        """ test commands ResetErrors """
        self.device.ResetErrors()
        self.controller.reset_errors.assert_called_once_with()

    def check_error(self, ctrl_attr_str, device_attr_str):
        """ check alarm attribute (value/quality), alarm state and alarm
        status """
        err_value = randrange(1, 20)
        err_msg = 'erreur {0} : {1}'.format(ctrl_attr_str, err_value)
        ctrl_attr = getattr(self.controller, ctrl_attr_str)
        ctrl_attr.return_value = (err_value, err_msg)
        device_attr = self.device.read_attribute(device_attr_str)
        # no alarm
        self.assertEqual(device_attr.value, 0)
        self.assertEqual(device_attr.quality, AttrQuality.ATTR_VALID)
        self.assertState(DevState.OFF)
        self.assertStatus("Device is in OFF state")
        sleep(READ_PERIOD)
        # set alarm
        device_attr = self.device.read_attribute(device_attr_str)
        self.assertEqual(device_attr.value, err_value)
        self.assertEqual(device_attr.quality, AttrQuality.ATTR_ALARM)
        self.assertState(DevState.ALARM)
        self.assertIn(err_msg, self.device.status())
        # reset alarm
        ctrl_attr.return_value = (0, "")
        sleep(READ_PERIOD)
        device_attr = self.device.read_attribute(device_attr_str)
        self.assertEqual(device_attr.value, 0)
        self.assertEqual(device_attr.quality, AttrQuality.ATTR_VALID)
        self.assertState(DevState.OFF)
        self.assertStatus("Device is in OFF state")

    def test_alarm_global_device_errors(self):
        """ test alarm attribute GlobalErrors """
        self.check_error("global_device_errors", "GlobalErrors")

    def test_alarm_ioni_supply_errors(self):
        """ test alarm attribute IoniSupplyErrors """
        self.check_error("ioni_supply_errors", "IoniSupplyErrors")

    def test_alarm_ioni_supply_warnings(self):
        """ test alarm attribute IoniSupplyWarnings """
        self.check_error("ioni_supply_warnings", "IoniSupplyWarnings")

    def test_alarm_voltage_supply_errors(self):
        """ test alarm attribute VoltageSupplyErrors """
        self.check_error("voltage_supply_errors", "VoltageSupplyErrors")

    def test_alarm_voltage_supply_warnings(self):
        """ test alarm attribute VoltageSupplyWarnings """
        self.check_error("voltage_supply_warnings", "VoltageSupplyWarnings")

    def test_pressure(self):
        with self.assertRaises(DevFailed) as context:
            self.device.Pressure
        expected_message = "It is currently not allowed to read attribute " \
                           "Pressure"
        self.assertIn(expected_message, str(context.exception))
        self.device.TurnOn()
        self.controller.is_on.return_value = True
        self.assertState(DevState.ON)
        self.assertEqual(self.device.Pressure, 1.65e-10)
        # test info msg
        self.controller.read_pressure.return_value = (
        1.5e-10, 0, "", 25, "info message")
        pressure_error = self.device.read_attribute("PressureError")
        self.assertEqual(pressure_error.value, 0)
        self.assertEqual(pressure_error.quality, AttrQuality.ATTR_VALID)
        self.assertEqual(self.device.Pressure, 1.5e-10)
        sleep(READ_PERIOD)
        self.assertIn("info message", self.device.status())
        # test alarm
        self.controller.read_pressure.return_value = (
        1.5e-6, 12, "alarm message", 25, "info message")
        self.assertEqual(self.device.Pressure, 1.5e-6)
        pressure_error = self.device.read_attribute("PressureError")
        self.assertEqual(pressure_error.value, 12)
        self.assertEqual(pressure_error.quality, AttrQuality.ATTR_ALARM)
        sleep(READ_PERIOD)
        self.assertIn("info message", self.device.status())
        self.assertIn("alarm message", self.device.status())
        self.assertState(DevState.ALARM)

    def test_emission_current(self):
        pass
