#!/usr/bin/env python

# Imports

from setuptools import setup

# Setup
setup(name="tangods-im540dev",
      version="0.2.2",
      author="Antoine Dupre",
      author_email="antoine.dupre@maxlab.lu.se",
      license="GPLv3",
      url="http://www.maxlab.lu.se",
      description="Device server for IM540 extractor gauge controller.",
      packages=['im540dev'],
      test_suite="nose.collector",
      scripts=['script/ExtractorGaugeIm540']
      )
