from PyTango.server import Device, DeviceMeta, attribute, command, run
from PyTango import AttrWriteType, DispLevel, DevState, DevUShort, DevULong
from PyTango import DebugIt
from PyTango.server import device_property
from im540dev.ressource import tick_context, catch_lib_error
from threading import Thread
from im540lib import Im540Controller, Im540Exception

debug_it = DebugIt(show_args=True, show_kwargs=True, show_ret=True)


class Im540Device(Device):
    """Tango Device for the im540 extractor gauge controller

    Device states description:
        - DevState.INIT : Device is not connected
        - DevState.OFF : Device emission is off
        - DevState.ON : Device emission is on
        - DevState.ALARM : Device add warning or error alarm
    """
    __metaclass__ = DeviceMeta

    # ------------------------------------------------------------------
    #    Properties
    # ------------------------------------------------------------------

    Host = device_property(
        dtype=str,
        default_value="192.168.127.254",
        doc="Device IP address")

    Port = device_property(
        dtype=int,
        default_value=4001,
        doc="Device port")

    ReadPeriod = device_property(
        dtype=float,
        default_value=0.0,
        doc="Period for updating error and state values")

    controller = None

    # ------------------------------------------------------------------
    #    General methods
    # ------------------------------------------------------------------

    @debug_it
    def delete_device(self):
        """Stop and join the thread."""
        self.running = False
        if self.thread and self.thread.isAlive():
            try:
                self.thread.join()
            except (RuntimeError, AttributeError) as e:
                self.info_stream("Shutdown internal update thread failed")
                self.debug_stream(str(e))
        try:
            self.controller.disconnect()
        except AttributeError as e:
            self.debug_stream(str(e))

    @debug_it
    def init_device(self):
        """Initializes the device."""
        Device.init_device(self)
        self.set_state(DevState.INIT)
        self.controller = Im540Controller(self.Host, self.Port)
        self._pressure_value = 0.
        self._pressure_errors_cde = 0
        self._pressure_errors_msg = ""
        self._global_errors_cde = 0
        self._global_errors_msg = ""
        self._ioni_errors_cde = 0
        self._ioni_errors_msg = ""
        self._ioni_warnings_cde = 0
        self._ioni_warnings_msg = ""
        self._voltage_errors_cde = 0
        self._voltage_errors_msg = ""
        self._voltage_warnings_cde = 0
        self._voltage_warnings_msg = ""
        self._pressure_info_msg = ""
        self._is_on = False
        self._emission_current = None
        self._emission_current_mode = ""
        self._sensor_ctl = 0
        self._sensor_ctl_mode = ""
        self._source = 0
        self._source_mode = ""
        self.running = True
        self.thread = None
        try:
            self._connect()
        except (IOError, Im540Exception) as e:
            self.error_stream(str(e))
            self.set_state(DevState.FAULT)
            self.set_status("Connection failed : \n{0}".format(e))

    @debug_it
    def update_pressure(self):
        """ Read pressure value, info, and errors """
        try:
            pressure, err, err_msg, _, info_msg = \
                self.controller.read_pressure(1)
            self._pressure_errors_cde = err
            self._pressure_errors_msg = err_msg
            self._pressure_info_msg = info_msg
            self._pressure_value = pressure
        except Im540Exception as ex:
            self._pressure_errors_msg = str(ex)
            self._pressure_errors_cde = ex.err_code
            raise ex

    def _update(self):
        if not self.get_state() in [DevState.UNKNOWN, DevState.INIT]:
            try:
                err, is_msg = self.check_errors()
                self.check_state(update_state=not err,
                                 erase_status=not bool(is_msg))
            except (Im540Exception, IOError) as ex:
                msg = 'Update :  communication error : {0}'
                msg = msg.format(str(ex))
                self.error_stream(msg)
                self.set_status(msg)
                self.set_state(DevState.FAULT)

    @debug_it
    def update_thread(self):
        """ Check error and update device state """
        while self.running:
            with tick_context(self.ReadPeriod):
                self._update()

    @debug_it
    def check_state(self, is_on=None, update_state=False, erase_status=False):
        """ Update the device state  (ON / OFF )"""
        if not is_on:
            is_on = self.controller.is_on()
        self._is_on = is_on
        if update_state:
            self.set_state(DevState.ON if is_on else DevState.OFF)
        if erase_status:
            self.set_status("Device is in {0} state".format(self.get_state()))

    @debug_it
    def check_errors(self):
        """ Update the device state and status in case of alarms """
        self.update_errors()
        err_msg = self.get_errors_msg()
        info_msg = self._pressure_info_msg
        if info_msg:
            info_msg = "\n".join(["Sensor infos ::", info_msg])
        if err_msg:
            err_msg = "\n".join(["Errors Report ::", err_msg, '', info_msg, ''])
            self.set_status(err_msg)
            self.set_state(DevState.ALARM)
            return True, err_msg
        self.set_status(info_msg)
        return False, info_msg

    @debug_it
    def get_errors_msg(self):
        """ Return all errors  message as a string """

        def merge_error(old_msg, new_msg):
            if new_msg:
                return "\n".join([old_msg, new_msg])
            return old_msg

        msg = ""
        msg = merge_error(msg, self._pressure_errors_msg)
        msg = merge_error(msg, self._global_errors_msg)
        msg = merge_error(msg, self._ioni_errors_msg)
        msg = merge_error(msg, self._voltage_errors_msg)
        msg = merge_error(msg, self._ioni_warnings_msg)
        msg = merge_error(msg, self._voltage_warnings_msg)
        return msg[1:]

    @debug_it
    def update_errors(self):
        """ Read all device errors """
        # global errors
        global_device_err = self.controller.global_device_errors()
        self._global_errors_cde, self._global_errors_msg = global_device_err
        # ioni errors
        ioni_err = self.controller.ioni_supply_errors()
        self._ioni_errors_cde, self._ioni_errors_msg = ioni_err
        # ioni warnings
        ioni_warn = self.controller.ioni_supply_warnings()
        self._ioni_warnings_cde, self._ioni_warnings_msg = ioni_warn
        # voltage errors
        voltage_err = self.controller.voltage_supply_errors()
        self._voltage_errors_cde, self._voltage_errors_msg = voltage_err
        # voltage warnings
        voltage_warn = self.controller.voltage_supply_warnings()
        self._voltage_warnings_cde, self._voltage_warnings_msg = voltage_warn

    # Commands ::

    @debug_it
    def _connect(self):
        """ Connection with device, start update thread """
        self.controller.connect()
        self.check_state(update_state=True, erase_status=True)
        self.running = True
        if self.ReadPeriod:
            self.thread = Thread(target=self.update_thread)
            self.thread.start()

    # ------------------------------------------------------------------
    #    Commands
    # ------------------------------------------------------------------

    @command
    @debug_it
    @catch_lib_error
    def Connect(self):
        """ Start communication"""
        try:
            self._connect()
        except RuntimeError as e:
            self.info_stream("internal thread is already runnig")
            self.debug_stream(str(e))

    @debug_it
    def is_Connect_allowed(self):
        return self.get_state() in [DevState.INIT, DevState.FAULT]

    @command(polling_period=1000)
    def Update(self):
        if not self.thread:
            self._update()

    @command
    @debug_it
    def Disconnect(self):
        """ Close connection """
        self.delete_device()
        self.set_state(DevState.INIT)
        self.set_status("Disconnected")

    def is_Disconnect_allowed(self):
        return self.get_state() not in [DevState.INIT]

    @command
    @debug_it
    @catch_lib_error
    def TurnOn(self):
        """ Send On command """
        _, is_on = self.controller.turn_on(1)
        update = not self.get_state() in [DevState.ALARM]
        self.check_state(is_on=is_on, update_state=update, erase_status=update)

    @debug_it
    def is_TurnOn_allowed(self):
        return self.get_state() not in [DevState.INIT]

    @command
    @debug_it
    @catch_lib_error
    def TurnOff(self):
        """ Send off command """
        _, is_on = self.controller.turn_off(1)
        update = not self.get_state() in [DevState.ALARM]
        self.check_state(is_on=is_on, update_state=update, erase_status=update)

    @debug_it
    def is_TurnOff_allowed(self):
        return self.get_state() not in [DevState.INIT]

    @command
    @debug_it
    @catch_lib_error
    def ResetErrors(self):
        """ Reset all device errors"""
        self.controller.reset_errors()

    @command(
        dtype_in=str,
        doc_in="Execute aribtrary command",
        dtype_out=str,
        doc_out="Returns the reply if a query,"
                "else DONE if suceeds, else TIMEOUT"
    )
    @debug_it
    @catch_lib_error
    def Execute(self, command):
        """Execute a custom command."""
        # Run command
        return str(self.controller.ask(command))
    # ------------------------------------------------------------------
    #    Attributes declaration
    # ------------------------------------------------------------------

    IsOn = attribute(
        label="Is on", dtype=bool,
        display_level=DispLevel.OPERATOR,
        access=AttrWriteType.READ,
        doc="Is device is turn on")

    Pressure = attribute(
        label="Pressure", dtype=float,
        display_level=DispLevel.OPERATOR,
        access=AttrWriteType.READ,
        unit="mBar", format="%e",
        doc="the sensor pressure")

    PressureError = attribute(
        label="Errors : Pressure", dtype=DevULong,
        display_level=DispLevel.OPERATOR,
        access=AttrWriteType.READ,
        max_alarm=1,
        doc="the sensor pressure errors code")

    EmissionCurrent = attribute(
        label="Emission current Value", dtype=DevUShort,
        display_level=DispLevel.OPERATOR,
        access=AttrWriteType.READ_WRITE,
        max_value=4,
        doc="the emission current value")

    EmissionCurrentMode = attribute(
        label="Emission current Mode", dtype=str,
        display_level=DispLevel.OPERATOR,
        access=AttrWriteType.READ,
        doc="the emission current mode (convert from Emission Current Value)")

    # Errors attributes
    GlobalErrors = attribute(
        label="Errors : Global", dtype=DevULong,
        display_level=DispLevel.OPERATOR,
        access=AttrWriteType.READ,
        max_alarm=1,
        doc="Global errors code")

    IoniSupplyErrors = attribute(
        label="Errors : Ioni Supply", dtype=DevULong,
        display_level=DispLevel.OPERATOR,
        access=AttrWriteType.READ,
        max_alarm=1,
        doc="Ioni supply errors code")

    IoniSupplyWarnings = attribute(
        label="Warnings : Ioni Supply", dtype=DevULong,
        display_level=DispLevel.OPERATOR,
        access=AttrWriteType.READ,
        max_alarm=1,
        doc="Ioni supply warnings code")

    VoltageSupplyErrors = attribute(
        label="Errors : Voltage Supply", dtype=DevULong,
        display_level=DispLevel.OPERATOR,
        access=AttrWriteType.READ,
        max_alarm=1,
        doc="Voltage supply errors code")

    VoltageSupplyWarnings = attribute(
        label="Warnings : Voltage Supply", dtype=DevULong,
        display_level=DispLevel.OPERATOR,
        access=AttrWriteType.READ,
        max_alarm=1,
        doc="Voltage supply warnings code")

    SensorSource = attribute(
        label="Sensor source value", dtype=DevUShort,
        display_level=DispLevel.EXPERT,
        min_value=0, max_value=4,
        access=AttrWriteType.READ_WRITE,
        doc="Type of sensor control")

    SensorSourceMode = attribute(
        label="Sensor source mode", dtype=str,
        display_level=DispLevel.EXPERT,
        access=AttrWriteType.READ,
        doc="Type of sensor control")

    SensorControl = attribute(
        label="Sensor control value", dtype=DevUShort,
        display_level=DispLevel.EXPERT,
        min_value=0, max_value=4,
        access=AttrWriteType.READ_WRITE,
        doc="sensor control mode")

    SensorControlMode = attribute(
        label="Sensor control mode", dtype=str,
        display_level=DispLevel.EXPERT,
        access=AttrWriteType.READ,
        doc="sensor control mode")

    AutoShutdownEmission = attribute(
        label="Auto shutdown emission", dtype=bool,
        display_level=DispLevel.EXPERT,
        access=AttrWriteType.READ_WRITE,
        doc="Auto shutdown emission on warning")
    # ------------------------------------------------------------------
    #   Attribute methods
    # ------------------------------------------------------------------

    # IsOn
    @debug_it
    def read_IsOn(self):
        return self._is_on

    @debug_it
    def is_IsOn_allowed(self, request):
        return self.get_state() not in [DevState.INIT, DevState.FAULT]

    # Presseure
    @debug_it
    def read_Pressure(self):
        self.update_pressure()
        return self._pressure_value

    @debug_it
    def is_Pressure_allowed(self, request):
        states = [DevState.INIT, DevState.FAULT]
        return self.get_state() not in states and self._is_on

    # PressureError
    @debug_it
    def read_PressureError(self):
        self.update_pressure()
        return self._pressure_errors_cde

    @debug_it
    def is_PressureError_allowed(self, request):
        return self.get_state() not in [DevState.INIT, DevState.FAULT]

    # EmissionCurrent
    @debug_it
    @catch_lib_error
    def read_EmissionCurrent(self):
        emission_current = self.controller.read_emission_current(1)
        self._emission_current, self._emission_current_mode = emission_current
        return self._emission_current

    @debug_it
    @catch_lib_error
    def write_EmissionCurrent(self, value):
        emission_current = self.controller.write_emission_current(1, value)
        self._emission_current, self._emission_current_mode = emission_current

    @debug_it
    def is_EmissionCurrent_allowed(self, request):
        if request is None or request == request.READ_REQ:
            return self.is_read_emission_current_allowed()
        return self.is_write_emission_current_allowed()

    @debug_it
    def is_write_emission_current_allowed(self):
        states = [DevState.INIT, DevState.ON, DevState.FAULT]
        return self.get_state() not in states

    @debug_it
    def is_read_emission_current_allowed(self):
        return self.get_state() not in [DevState.INIT, DevState.FAULT]

    # EmissionCurrentMode
    @debug_it
    def read_EmissionCurrentMode(self):
        return self._emission_current_mode

    @debug_it
    def is_EmissionCurrentMode_allowed(self, request):
        return self.is_read_emission_current_allowed()

    # Errors ::
    # GlobalErrors
    @debug_it
    def read_GlobalErrors(self):
        return self._global_errors_cde

    @debug_it
    def is_GlobalErrors_allowed(self, request):
        return self.get_state() not in [DevState.INIT, DevState.FAULT]

    # IoniSupplyErrors
    @debug_it
    def read_IoniSupplyErrors(self):
        return self._ioni_errors_cde

    @debug_it
    def is_IoniSupplyErrors_allowed(self, request):
        return self.get_state() not in [DevState.INIT, DevState.FAULT]

    # IoniSupplyWarnings
    @debug_it
    def read_IoniSupplyWarnings(self):
        return self._ioni_warnings_cde

    @debug_it
    def is_IoniSupplyWarnings_allowed(self, request):
        return self.get_state() not in [DevState.INIT, DevState.FAULT]

    # VoltageSupplyErrors
    @debug_it
    def read_VoltageSupplyErrors(self):
        return self._voltage_errors_cde

    @debug_it
    def is_VoltageSupplyErrors_allowed(self, request):
        return self.get_state() not in [DevState.INIT, DevState.FAULT]

    # VoltageSupplyWarnings
    @debug_it
    def read_VoltageSupplyWarnings(self):
        return self._voltage_warnings_cde

    @debug_it
    def is_VoltageSupplyWarnings_allowed(self, request):
        return self.get_state() not in [DevState.INIT, DevState.FAULT]

    # SensorSource

    @debug_it
    def write_SensorSource(self, value):
        values = self.controller.write_sensor_control_source(value)
        self._source, self._source_mode = values

    @debug_it
    def read_SensorSource(self):
        values = self.controller.read_sensor_control_source()
        self._source, self._source_mode = values
        return self._source

    @debug_it
    def is_SensorSource_allowed(self, request):
        states = [DevState.INIT, DevState.FAULT]
        return self.get_state() not in states

    @debug_it
    def read_SensorSourceMode(self):
        return self._source_mode

    @debug_it
    def is_SensorSourceMode_allowed(self, request):
        states = [DevState.INIT, DevState.FAULT]
        return self.get_state() not in states

    # SensorControl

    @debug_it
    def write_SensorControl(self, value):
        values = self.controller.write_sensor_control_mode(channel=1,
                                                           mode=value)
        self._sensor_ctl, self._sensor_ctl_mode = values
        return self._sensor_ctl

    @debug_it
    def read_SensorControl(self):
        values = self.controller.read_sensor_control_mode(channel=1)
        self._sensor_ctl, self._sensor_ctl_mode = values
        return self._sensor_ctl

    @debug_it
    def is_SensorControl_allowed(self, request):
        states = [DevState.INIT, DevState.FAULT]
        return self.get_state() not in states

    @debug_it
    def read_SensorControlMode(self):
        return self._sensor_ctl_mode

    @debug_it
    def is_SensorControlMode_allowed(self, request):
        states = [DevState.INIT, DevState.FAULT]
        return self.get_state() not in states

    # AutoShutdownEmission
    @debug_it
    def write_AutoShutdownEmission(self, request):
        self.controller.set_disable_emission_on_warning(request)

    @debug_it
    def read_AutoShutdownEmission(self):
        return self.controller.is_emission_disabled_on_warning()

    @debug_it
    def is_AutoShutdownEmission_allowed(self, request):
        states = [DevState.INIT, DevState.FAULT]
        return self.get_state() not in states

    UsedThread = attribute(
        label="Used internal update thread", dtype=bool,
        display_level=DispLevel.EXPERT,
        access=AttrWriteType.READ,
        doc="Used internal update thread")

    def read_UsedThread(self):
        return self.thread is not None and self.thread.isAlive()


if __name__ == "__main__":
    import sys
    sys.argv[0] = "ExtractorGaugeIm540"
    run([Im540Device])
