"""Module to run the server."""

# Imports
import sys
from PyTango import server
from im540dev.device import Im540Device


#: Server name as used in the Tango database
SERVER_NAME = "ExtractorGaugeIm540"


# Run function
def run(args=None, **kwargs):
    if not args:
        args = sys.argv[1:]
    args = [SERVER_NAME] + list(args)
    server.run((Im540Device,), args, **kwargs)

# Main execution
if __name__ == "__main__":
    run()