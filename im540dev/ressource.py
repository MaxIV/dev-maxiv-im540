from time import sleep
from contextlib import contextmanager
from timeit import default_timer as time
from functools import wraps
from PyTango import DevState, Except
from im540lib import Im540Exception

# Tick context
@contextmanager
def tick_context(value):
    """Generate a context that controls the duration of its execution."""
    start = time()
    yield
    sleep_time = start + value - time()
    if sleep_time > 0:
        sleep(sleep_time)


def catch_lib_error(func):
    """Decorator to format communication errors."""

    @wraps(func)
    def wrapper(self, *args, **kwargs):
        try:
            return func(self, *args, **kwargs)
        except (IOError, Im540Exception) as exc:
            origin = str(type(self)) + "::" + func.__name__
            self.error_stream('communication error : {0}, {1}'.format(str(exc), origin))
            Except.throw_exception('COMMUNICATION_ERROR', str(exc), origin)

    return wrapper
